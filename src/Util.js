import { firebase } from "@firebase/app";
import "@firebase/auth";
import "@firebase/database";
import "firebase/firestore";
import "firebase/storage";


export const encodePhone = phone => {
    if (phone) {
      return phone.replace("+", "");
    } else {
      return "phone invalid";
    }
  };


export  const  encodeEmail = email => {
    if (email) {
      return email.replace(".", "_");
    } else {
      return "Email invalid";
    }
  };

  export const createCookie = (value, days) => {
    var expires = "";
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
      expires = date.toGMTString();
    } else expires = "";
    document.cookie = `userEmail=${value};expires=${expires};path=/`;
    console.log(document.cookie);
  };

  export const readCookie = () => {
    var nameEQ = "userEmail=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === " ") c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) === 0) {
        return c.substring(nameEQ.length, c.length);
      }
    }
    return null;
  };


  export const getUserData = async (user) => {
    console.log("getserData: " + user);
    let promise = firebase
    .database()
    .ref(`users/${user}`)
    .once("value");
    return promise;
  };

  export const getUserPhoneData = async (phone) => {
    console.log("getuserPhoneData: " + phone);
    let promise = firebase
    .database()
    .ref(`users/${phone}`)
    .once("value");
    return promise;
  };
  
  
