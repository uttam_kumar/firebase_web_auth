import React, { Component } from "react";

import { firebase } from "@firebase/app";
import "@firebase/auth";
import "@firebase/database";
import "firebase/firestore";
import "firebase/storage";
import ChangePassword from "./ChangePassword";
import {encodeEmail, readCookie, getUserData} from "./Util"

export default class UserSecurity extends Component {
  state = {
    showPasswordChangeForm: false,
    checked: false,
    userEmail: ""
  };

  componentDidMount() {
    this.checkingTwoFactorAuthEnabled();
  }

  checkingTwoFactorAuthEnabled = () => {
    this.setState({ userEmail: readCookie() }, () => {
      let promise = getUserData(encodeEmail(this.state.userEmail));
      promise.then(async snapshot => {
        var isEnabledTwoFactorAuth = false;
        if (snapshot.val() && snapshot.val().enableTwoFactorAuth) {
          isEnabledTwoFactorAuth = snapshot.val().enableTwoFactorAuth;
        }
        this.setState({
          checked: isEnabledTwoFactorAuth
        });
        if (isEnabledTwoFactorAuth) {
          document.getElementById("checkbox").checked = true;
        } else {
          document.getElementById("checkbox").checked = false;
        }
      });
    });
  };

 
  changePasswordBtnClick = () => {
    this.setState({
      showPasswordChangeForm: !this.state.showPasswordChangeForm
    });
  };

  //enable when need
  handleCheckBoxClick = () => {
    if (this.state.checked) {
      this.setState({ checked: false }, () => {
        this.setTwoFactorAuthForUser();
      });
    } else {
      this.setState({ checked: true }, () => {
        this.setTwoFactorAuthForUser();
      });
    }
  };

  setTwoFactorAuthForUser = () => {
    console.log(this.state.userEmail);
    var userEmail = encodeEmail(this.state.userEmail);
    firebase
      .database()
      .ref(`users/${userEmail}`)
      .update({
        enableTwoFactorAuth: this.state.checked
      });
  };

  newPAssCancelBtnClick = () => {
    this.setState({
      showPasswordChangeForm: false
    });
    console.log("cancel clicked");
  };

  render() {
    return (
      <React.Fragment>
        <div>
          {!this.state.showPasswordChangeForm && (
            <React.Fragment>
              <h1 className="large bold title-margin">User Security</h1>
              <button onClick={this.changePasswordBtnClick}>
                Change Password
              </button>
              <div>
                <span>
                  Two factor authentication:
                </span>
                <input
                  type="checkbox"
                  onChange={this.handleCheckBoxClick}
                  className="filled-in"
                  id="checkbox"
                />
                <br />
                <br />
                <button onClick={this.props.handleSecurity}>
                  Back to main page
                </button>
                <br />
                <br />
              </div>
            </React.Fragment>
          )}
        </div>
        {this.state.showPasswordChangeForm && (
          <div>
            {/* change here when you need 2 auth functionality */}
            <ChangePassword
              changePasswordBtnClick={this.changePasswordBtnClick}
               checkingTwoFactorAuthEnabled={this.checkingTwoFactorAuthEnabled
               }
            />
          </div>
        )}
      </React.Fragment>
    );
  }
}
