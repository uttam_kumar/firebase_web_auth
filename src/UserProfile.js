import React, { Component } from "react";

import { firebase } from "@firebase/app";
import "@firebase/auth";
import "@firebase/database";
import "firebase/firestore";
import "firebase/storage";

import {encodeEmail, createCookie, readCookie, getUserData} from "./Util"

export default class UserProfile extends Component {
  state = {
    profilePhotoUrl: "",
    profilePhotoName: "",
    userEmail: "",
    Phone: "",
    Name: "",
    currentName: "",
    currentPhone: "",
    showNameEditForm: false,
    showPhoneEditForm: false,
    showImageUpdateForm: false,
    selectedFile: null,
    mainImage: null,
    newImage: ""
  };

  handleDisplayImage = () => {
    var userEmail = encodeEmail(this.state.userEmail);

    let promise = getUserData(userEmail);
    promise.then(async snapshot => {
      var imageName =
        (snapshot.val() && snapshot.val().imageName) || "Anonymous";
      console.log(imageName);
      this.setState(
        {
          profilePhotoName: imageName
        },
        () => {
          this.getImage();
        }
      );
    });
  };

  getImage() {
    let image = this.state.profilePhotoName;
    let storage = firebase.storage().ref();
    storage
      .child(`images/${image}`)
      .getDownloadURL()
      .then(url => {
        this.setState({
          profilePhotoUrl: url
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleLogout = () => {
    createCookie("", -1);
    firebase
      .auth()
      .signOut()
      .then(() => {
        this.setState({ userEmail: "" }, () => {
          createCookie("", -1);
          this.props.changeLoginState(0);
        });
      })
      .catch(err => {
        console.error(err);
      });
  };

  componentDidMount() {
    this.setState({ userEmail: readCookie() }, () => {
      console.log("read cookie is called");
      let promise = getUserData(encodeEmail(this.state.userEmail));
      promise.then(async snapshot => {

        this.handleDisplayImage();
        this.setState({
          currentName: snapshot.val().name,
          currentPhone: snapshot.val().phone
        });
      });
    });
  }

  editPhoneNumberBtnClick = () => {
    this.setState({
      showPhoneEditForm: true,
      showNameEditForm: false
    });
    console.log("editPhoneNumberBtnClick");
  };

  editPhoneSaveBtnClick = () => {
    if (this.state.Phone) {
      this.updatePhoneNumber();
    }
  };

  editPhoneCancelBtnClick = () => {
    this.setState({
      showPhoneEditForm: false
    });
  };

  handleNewPhoneNumber = e => {
    this.setState({ Phone: e.target.value });
  };

  updatePhoneNumber = () => {
    console.log(this.state.Phone);
    var userEmail = encodeEmail(this.state.userEmail);
    firebase
      .database()
      .ref(`users/${userEmail}`)
      .update({
        phone: this.state.Phone
      });
    this.setState({
      showPhoneEditForm: false,
      currentPhone: this.state.Phone
    });
  };

  editNameBtnClick = () => {
    this.setState({
      showNameEditForm: true,
      showPhoneEditForm: false
    });
    console.log("editNameBtnClick");
  };

  editNameSaveBtnClick = () => {
    console.log("this.state.Name");
    if (this.state.Name) {
      this.updateNewName();
    }
  };

  editNameCancelBtnClick = () => {
    this.setState({
      showNameEditForm: false
    });
  };

  handleNewName = e => {
    this.setState({ Name: e.target.value });
  };

  updateNewName = () => {
    console.log(this.state.Name);

    var userEmail = encodeEmail(this.state.userEmail);
    firebase
      .database()
      .ref(`users/${userEmail}`)
      .update({
        name: this.state.Name
      });
    this.setState({
      showNameEditForm: false,
      currentName: this.state.Name
    });
  };

  updateImageBtnClick = () => {
    this.setState({
      showImageUpdateForm: true
    });
  };

  handleFile = e => {
    this.setState({ selectedFile: e.target.files[0], loaded: 0 }, () => {
      const fileElem = this.inputElm.files[0];

      let file = fileElem;
      const stroageRef = firebase.storage().ref("/images");
      var fileName = Date.now();
      var fileNameEx = fileName + "." + file.type.split("/")[1];
      const mainImage = stroageRef.child(fileNameEx);
      this.setState({ mainImage });
      this.setState({
        newImage: fileNameEx
      });
    });
  };

  updateImageNameOnDatabase = fileNameEx => {
    var userEmail = encodeEmail(this.state.userEmail);
    console.log(userEmail);
    firebase
      .database()
      .ref(`users/${userEmail}`)
      .update({
        imageName: fileNameEx
      });
      this.setState({
        showImageUpdateForm: false
      });
      this.handleDisplayImage()
  };

  updateImageCancelBtnClick = () => {
    this.setState({
      showImageUpdateForm: false
    });
  };

  updateImageSaveBtnClick = () => {
    if (this.state.selectedFile !== null) {
      this.state.mainImage.put(this.state.selectedFile).then(
        res => {
          if (res.state === "success"){
            this.updateImageNameOnDatabase(this.state.newImage);
          }else{
            console.log("error to upload image");
          }          
        }
      );
    } else {
      console.log("no file selected");
    }
  };

  render() {
    return (
      <React.Fragment>
        <h1 className="large bold title-margin">User Profile</h1>
        <br />
        <div>
          <img
            height="150px"
            width="250px"
            src={this.state.profilePhotoUrl}
            alt=""
          />

          {!this.state.showImageUpdateForm && (
            <button onClick={this.updateImageBtnClick}>Update Image</button>
          )}
          {this.state.showImageUpdateForm && (
            <div>
              <span>Update Image</span>
              <br />
              <input
                className="small-margin"
                id="file-up"
                type="file"
                onChange={this.handleFile}
                ref={inpElm => (this.inputElm = inpElm)}
              />
              <br />
              <button onClick={this.updateImageSaveBtnClick}>Save</button>
              <button onClick={this.updateImageCancelBtnClick}>Cancel</button>
            </div>
          )}
        </div>
        <br />
        <div>
          Name : {this.state.currentName}
          {!this.state.showNameEditForm && (
            <button onClick={this.editNameBtnClick}>Edit Name</button>
          )}
        </div>

        <br />
        {this.state.showNameEditForm && (
          <div>
            <span>Enter your name here:</span>
            <br />
            <input onChange={this.handleNewName} />
            <br />
            <button onClick={this.editNameSaveBtnClick}>Save</button>
            <button onClick={this.editNameCancelBtnClick}>Cancel</button>
          </div>
        )}
        <br />
        <div>
          Phone : {this.state.currentPhone}{" "}
          {!this.state.showPhoneEditForm && (
            <button onClick={this.editPhoneNumberBtnClick}>Edit Phone</button>
          )}
        </div>

        <br />
        {this.state.showPhoneEditForm && (
          <div>
            <span>Enter new phone number here:</span>
            <br />
            <input onChange={this.handleNewPhoneNumber} />
            <br />
            <button onClick={this.editPhoneSaveBtnClick}>Save</button>
            <button onClick={this.editPhoneCancelBtnClick}>Cancel</button>
          </div>
        )}
        <br />
        <button onClick={this.props.handleUserProfile}>
          Back to main page
        </button>
        <br />
      </React.Fragment>
    );
  }
}
