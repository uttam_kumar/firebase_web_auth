import React, { Component } from "react";

import { firebase } from "@firebase/app";
import "@firebase/auth";
import "@firebase/database";
import "firebase/firestore";
import "@firebase/storage";

import {encodeEmail, createCookie, encodePhone} from "./Util"

export default class SignUp extends Component {
  state = {
    email: "",
    passWord: "",
    signUpFailedMsg: "",
    phone: "",
    nick: "",
    selectedFile: null,
    mainImage: null,
    fileNameEx: ""
  };

  handleEmail = e => {
    this.setState({ email: e.target.value });
  };

  handlePassword = e => {
    this.setState({ passWord: e.target.value });
  };

  handlePhone = e => {
    this.setState({ phone: e.target.value });
  };

  handleNick = e => {
    this.setState({ nick: e.target.value });
  };

  handleFile = e => {
    this.setState({ selectedFile: e.target.files[0], loaded: 0 }, () => {
      const fileElem = this.inputElm.files[0];

      let file = fileElem;
      const stroageRef = firebase.storage().ref("/images");
      var fileName = Date.now();
      var fileNameEx = fileName + "." + file.type.split("/")[1];
      this.setState({ fileNameEx });
      const mainImage = stroageRef.child(fileNameEx);

      this.setState({ mainImage });
    });
  };

  componentWillUnmount() {
    this.fireBaseListener && this.fireBaseListener();
    this.authListener = undefined;
  }

  writeUserData = (ecodedEmail, email, name, phoneNumber) => {
    if (this.state.mainImage !== null) {
      this.state.mainImage.put(this.state.selectedFile).then(res => {
        console.log(res);
        if (res.state === "success") {
          var sData = firebase
            .database()
            .ref(`users/${ecodedEmail}`)
            .set(
              {
                email: email,
                name: name,
                phone: phoneNumber,
                imageName: this.state.fileNameEx,
                enableTwoFactorAuth: false
              },
              error => {
                //delete user form here
                if (error) {
                  console.log("error to create account");
                  //this.deleteUser();
                } else if (!error) {
                  this.writePhoneData();
                  createCookie(email, 1);
                  console.log("successfully account created " + sData);
                }
              }
            );
          //createCookie(this.state.email, 1);
        } else {
          console.log("error to upload image/ error to create account");
        }
      });
      //error to store data to firebase db delele user from auth
    } else {
      console.log("no image");
    }
  };

  validateEmail = mail => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return true;
    }
    return false;
  };

  handleSignUp = async () => {
    createCookie("", -1);
    console.log("create cookie called");
    if (
      this.state.email !== "" &&
      this.state.passWord !== "" &&
      this.state.phone !== "" &&
      this.state.selectedFile !== null
    ) {
      if (this.validateEmail(this.state.email)) {
        let signUpData = await firebase
          .auth()
          .createUserWithEmailAndPassword(this.state.email, this.state.passWord)
          .catch(error => {
            if (error) {
              alert("can not register with this username / password");
            } else {
              console.log("successfully sign up");
            }
          });

        if (signUpData) {
          createCookie(this.state.email, 1);
          //uploading user data to database
          this.writeUserData(
            encodeEmail(this.state.email),
            this.state.email,
            this.state.nick,
            this.state.phone
          );
        } else {
          //alert("somethings went wrong");
        }
      } else {
        alert("Please input a valid Email!");
      }
    } else {
      alert("input fields cant be empty");
    }
  };

  // deleteUser = () => {
  //   var user = firebase.auth().currentUser;
  //   user
  //     .delete()
  //     .then(() => {
  //       console.log("deleted user");
  //       // User deleted.
  //     })
  //     .catch(error => {
  //       // An error happened.
  //     });
  // };

  writeUserPhoneNumber = (phone, email) => {
    var ref = firebase
      .database()
      .ref(`users/${phone}`)
      var newUserRef = ref.push();
      newUserRef.set({ email: email });
  };


  getUserPhoneData = async () => {
    let promise = this.readPhoneData(encodePhone(this.state.phone));
    return promise;
  };

  readPhoneData = phone => {
    return firebase
      .database()
      .ref(`users/${phone}`)
      .once("value");
  };

  writePhoneData = () => {
    var prom = this.getUserPhoneData();
    prom.then(async snapshot => {
      //if(snapshot.numChildren())
      this.writeUserPhoneNumber(
        encodePhone(this.state.phone),
        this.state.email
      );
    });
  };

  render() {
    return (
      <React.Fragment>
        <h1 className="large bold title-margin">Sign up </h1>
        <span className="small-margin small-mr">Email : </span>
        <input
          className="small-margin"
          type="text"
          onChange={this.handleEmail}
        />
        <br />
        <span className="small-margin">Password : </span>
        <input
          className="small-margin"
          type="password"
          onChange={this.handlePassword}
        />
        <br />
        <span className="small-margin small-mr">Phone: </span>
        <input
          className="small-margin"
          type="text"
          onChange={this.handlePhone}
        />
        <br />
        <span className="small-margin">NickName : </span>
        <input
          className="small-margin"
          type="text"
          onChange={this.handleNick}
        />
        <br />
        <span className="small-margin small-mr">Image : </span>
        <input
          className="small-margin"
          id="file-up"
          type="file"
          onChange={this.handleFile}
          ref={inpElm => (this.inputElm = inpElm)}
        />
        <br />
        <button onClick={this.handleSignUp}>Sign Up</button>
      </React.Fragment>
    );
  }
}
