import React, { Component } from "react";

import { firebase } from "@firebase/app";
import "@firebase/auth";
import "@firebase/database";
import "firebase/firestore";
import "firebase/storage";

import UserProfile from "./UserProfile";
import UserSecurity from "./UserSecurity";
import {encodeEmail, createCookie, getUserData} from "./Util"

export default class MainPage extends Component {
  state = {
    userEmail: "",
    showUserProfile: false,
    showSecurity: false,
    promptEmailVerification: true
  };

  displayUserData = () => {
    var prom = getUserData(encodeEmail(this.state.userEmail));
    prom.then(async snapshot => {
      if (snapshot.val()) {
        console.log(snapshot.val());
      }
    });
  };

  handleLogout = () => {
    createCookie("", -1);
    firebase
      .auth()
      .signOut()
      .then(() => {
        this.setState({ userEmail: "" }, () => {
          createCookie("", -1);
          this.props.changeLoginState(0);
        });
      })
      .catch(err => {
        console.error(err);
      });
  };

  handleUserProfile = () => {
    this.setState({
      showUserProfile: !this.state.showUserProfile
    });
  };

  handleSecurity = () => {
    this.setState({
      showSecurity: !this.state.showSecurity
    });
  };

  componentDidMount() {
    this.setState({
      promptEmailVerification: !firebase.auth().currentUser.emailVerified
    });
    getUserData(encodeEmail(this.state.userEmail));
  }

  sendEmailVerificationLink = () => {
    var user = firebase.auth().currentUser;
    user
      .sendEmailVerification()
      .then(() => {
        // Email sent.
      })
      .catch(error => {
        console.log(error);
        // An error happened.
      });
  };

  testEmailVerification = () => {
    firebase
      .auth()
      .currentUser.reload()
      .then(() => {
        let emailVerified = firebase.auth().currentUser.emailVerified;
        if (emailVerified === false) {
          console.log("you did not click the link");
        } else {
          console.log("successfully verified email");
        }
        this.setState({
          promptEmailVerification: !emailVerified
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    const emailVerificationJSX = (
      <div>
        <div className="bold">
          Your email address is not verified. Please verify:
        </div>
        <div>
          <br />
          <button className="bold" onClick={this.sendEmailVerificationLink}>
            Send Verification Link to your email
          </button>
        </div>
        <br />
        <button onClick={this.testEmailVerification}>
          Test your email verification
        </button>
        <br />
        <br />
      </div>
    );

    return (
      <React.Fragment>
        {!this.state.showUserProfile && !this.state.showSecurity && (
          <div>
            <h1 className="large bold title-margin">Successfully Logged in</h1>
            <br />
            <button onClick={this.handleUserProfile}>User Profile</button>
            <br />
            <br />
            <button onClick={this.handleSecurity}>Security</button>
            <br />
            <br />
            <button onClick={this.handleLogout}>Logout</button>
          </div>
        )}

        {this.state.showUserProfile && (
          <UserProfile handleUserProfile={this.handleUserProfile} />
        )}
        {this.state.showSecurity && (
          <UserSecurity handleSecurity={this.handleSecurity} />
        )}

        {this.state.promptEmailVerification ? emailVerificationJSX : null}
      </React.Fragment>
    );
  }
}
