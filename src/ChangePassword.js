import React, { Component } from "react";

import { firebase } from "@firebase/app";
import "@firebase/auth";
import "@firebase/database";
import "firebase/firestore";
import "firebase/storage";
import {encodeEmail, createCookie, readCookie, getUserData} from "./Util"

export default class ChangePassword extends Component {
  state = {
    showPasswordChangeForm: true,
    oldPassword: "",
    newPassword: "",
    confirmPassword: "",
    userEmail: "",
    changePassStatus: ""
  };

  componentDidMount() {
    this.setState({ userEmail: readCookie() }, () => {
      console.log("read cookie is called");
      getUserData(encodeEmail(this.state.userEmail));
    });
  }

  changePasswordBtnClick = () => {
    this.setState({
      showPasswordChangeForm: true,
      showTwoAuthSettingForm: false
    });
  };

  newPassSaveBtnClick = () => {
    if (this.state.oldPassword && this.state.newPassword) {
      if (this.state.newPassword === this.state.confirmPassword) {
        if (this.state.newPassword.length > 5) {
          this.checkOldPasswordCorrect();
        } else {
          console.log("enter minimum 6 digits password");
          this.setState({
            changePassStatus: "enter minimum 6 digits password"
          })
        }
      } else {
        console.log("confirm pass did not match");
        this.setState({
          changePassStatus: "confirm pass did not match"
        })
      }
    } else {
      console.log("empty field");
      this.setState({
        changePassStatus: "empty field"
      })
    }
  };

  upadteNewPassword = () => {
    var user = firebase.auth().currentUser;

    user
      .updatePassword(this.state.newPassword)
      .then(() => {
        // Update successful.
        createCookie(user.email, 1);
        console.log("password updated");
        this.setState({
          changePassStatus: "password updated"
        })
        this.props.changePasswordBtnClick();
        //change here when you need 2 auth functionality
        this.props.checkingTwoFactorAuthEnabled();
      })
      .catch(error => {
        this.setState({
          changePassStatus: "error to update password"
        })
        console.log("error to update password");
      });
  };

  checkOldPasswordCorrect = async () => {
    let loginData = await firebase
      .auth()
      .signInWithEmailAndPassword(this.state.userEmail, this.state.oldPassword)
      .catch(error => {
        this.setState({
          changePassStatus: "wrong uname/password"
        })
        console.log("wrong uname/password");
      });
    console.log(loginData);
    if (loginData) {
      this.upadteNewPassword();
    }
  };

  newPAssCancelBtnClick = () => {
    this.props.changePasswordBtnClick();
    //change here when you need 2 auth functionality
    this.props.checkingTwoFactorAuthEnabled();
    console.log("cancel clicked");
  };

  handleOldPassword = e => {
    this.setState({ oldPassword: e.target.value });
  };

  handleNewPassword = e => {
    this.setState({ newPassword: e.target.value });
  };

  handleConfirmPassword = e => {
    this.setState({ confirmPassword: e.target.value });
  };

  render() {
    return (
      <React.Fragment>
        {this.state.showPasswordChangeForm && (
          <div>
            <h1 className="large bold title-margin">Change Password</h1>
            Email : {this.state.userEmail}
            <br />
            <br />
            <span>Enter old password:</span>
            <br />
            <input onChange={this.handleOldPassword} />
            <br />
            <br />
            <span>Enter new password:</span>
            <br />
            <input onChange={this.handleNewPassword} />
            <br />
            <br />
            <span>Confirm new password:</span>
            <br />
            <input onChange={this.handleConfirmPassword} />
            <br />
            <br />
            <button onClick={this.newPassSaveBtnClick}>Save</button>
            <button onClick={this.newPAssCancelBtnClick}>Cancel</button>
            <div>{this.state.changePassStatus}</div>
          </div>
        )}
        <br />
      </React.Fragment>
    );
  }
}
