import React, { Component } from "react";

import { firebase } from "@firebase/app";
import "@firebase/auth";
//import { contains } from "@firebase/util";
import ForgotEmail from "./ForgotEmail";
import {encodeEmail, createCookie} from "./Util"

//changeLoginState(0) => app.js
//changeLoginState(1) => mainpage.js
//changeLoginState(2) => loading.js
//changeLoginState(3) => phoneAuth.js
export default class Login extends Component {
  state = {
    userEmail: "",
    passWord: "",
    forgotPasswordForm: false,
    forgotEmailForm: false,
    sendOTPhone: ""
  };

  componentDidMount = () => {
    this.setState({ userEmail: "" });
  };

  componentWillUnmount() {
    this.fireBaseListener && this.fireBaseListener();
    this.authListener = undefined;
  }

  handleEmail = e => {
    this.setState({ userEmail: e.target.value });
  };

  handlePassword = e => {
    this.setState({ passWord: e.target.value });
  };

  handleLogin = async () => {
    this.props.changeLoginState(2); //loading..
    if (this.state.userEmail !== "" && this.state.passWord !== "") {
      let loginData = await firebase
        .auth()
        .signInWithEmailAndPassword(this.state.userEmail, this.state.passWord)
        .catch(error => {
          alert("wrong uname/password");
          this.props.changeLoginState(0);
        });
      console.log(loginData);
      if (loginData) {
        createCookie(this.state.userEmail, 1);
      }
    } else {
      alert("input fields cant be empty");
      this.props.changeLoginState(0);
    }
  };

  sendEmailForgotPassword = emailAddress => {
    var auth = firebase.auth();
    auth
      .sendPasswordResetEmail(emailAddress)
      .then(() => {
        this.setState({
          forgotPasswordForm: false
        });
        alert("Email sent to: " + emailAddress);
      })
      .catch(error => {
        alert("Something went wrong/ wrong email");
      });
  };

  handleForgotPassword = () => {
    this.setState({
      forgotPasswordForm: true,
      forgotEmailForm: false
    });
  };

  handleSendEmail = () => {
    console.log(this.state.emailSend);
    if (this.state.emailSend) {
      this.sendEmailForgotPassword(this.state.emailSend);
    }
  };

  sendEmailCancel = () => {
    this.setState({
      forgotPasswordForm: false
    });
  };

  handleForgotPasswordInput = e => {
    this.setState({ emailSend: e.target.value });
  };

  handleForgotEmail = item => {
    console.log("clicked");
    console.log(item);

    this.setState({
      forgotEmailForm: !this.state.forgotEmailForm,
      forgotPasswordForm: false,
      userEmail: item
    });
  };

  render() {
    return (
      <React.Fragment>
        {!this.state.forgotEmailForm && (
          <div>
            <h1 className="large bold title-margin">
              Login using firebase auth:
            </h1>
            <span className="small-margin small-mr">Email : </span>
            <input
              className="small-margin"
              type="text"
              value={this.state.userEmail}
              onChange={this.handleEmail}
            />
            <br />
            <span className="small-margin">Password : </span>
            <input
              className="small-margin"
              type="password"
              onChange={this.handlePassword}
            />
            <br />
            <button
              className="small-margin"
              onClick={this.handleForgotPassword}
            >
              Forgot Password
            </button>
            <button className="small-margin" onClick={this.handleForgotEmail}>
              Forgot Email
            </button>
            <button className="small-margin" onClick={this.handleLogin}>
              Login
            </button>
          </div>
        )}
        {this.state.forgotPasswordForm && (
          <div>
            <span>Enter email here:</span>
            <br />
            <input onChange={this.handleForgotPasswordInput} />
            <br />
            <button onClick={this.handleSendEmail}>Send email</button>
            <button onClick={this.sendEmailCancel}>Cancel</button>
          </div>
        )}
        {this.state.forgotEmailForm && (
          <div>
            <ForgotEmail handleForgotEmail={this.handleForgotEmail} />
          </div>
        )}
      </React.Fragment>
    );
  }
}
