import React, { Component } from "react";

import { firebase } from "@firebase/app";
import "@firebase/auth";
import "@firebase/database";
import "firebase/firestore";
import "firebase/storage";

import {encodePhone, getUserPhoneData} from "./Util"

var appVerifier = "";

export default class PhoneAuth extends Component {
  state = {
    phoneNumber: "",
    confirmationResult: null,
    vCode: "",
    isCodeSent: false,
    isVerifySuccess: false,
    mailList: []
  };


  displayPhoneData = () => {
    var prom = getUserPhoneData(encodePhone(this.state.phoneNumber));
    prom.then(async snapshot => {
      if (snapshot.val()) {
        //when data match
        console.log(snapshot.val());
        let mailArray = [];
        snapshot.forEach(childSnapshot => {
          mailArray.push(childSnapshot.val().email);
          console.log(childSnapshot.val().email);
        });
        this.setState({
          mailList: mailArray
        });
      } else {
        //when data do not match
        console.log("no user");
      }
    });
  };

  //get otpcode textbox text
  handleVCode = e => {
    this.setState({ vCode: e.target.value });
  };

  //get phone textbox text
  handlePhoneNumber = e => {
    this.setState({ phoneNumber: e.target.value });
  };

  componentWillUnmount() {
    this.fireBaseListener && this.fireBaseListener();
    this.authListener = undefined;
  }

  componentDidMount() {}

  //verifying rechaptcha
  rechaptchaVerify = () => {
    //handle rechaptcha on invisibly
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
      "recaptcha-container",
      {
        size: "invisible",
        callback: response => {
          // reCAPTCHA solved, allow signInWithPhoneNumber.
          console.log(response);
        },
        "expired-callback": () => {
          //if expired
          console.log("error callback");
          appVerifier = ""
        },
        "error-callback": () => {
          //if error
          console.log("error callback");
          appVerifier = ""
        }
      }
    );

    window.recaptchaVerifier.render().then(r => {
      window.recaptchaWidgetId = r;
    });

    return window.recaptchaVerifier;
  };

  //sending otp code to user phone
  sendCode = async () => {
    if (this.state.phoneNumber.length > 8) {
      console.log(appVerifier)
      //controlling execution of rechaptchaVerifier() is very very important here.
      //if not conlrolled then makes render and shows error when phone number is wrong with from 2nd time 
      if (!appVerifier) {
        appVerifier = this.rechaptchaVerify();
      }

      if (this.state.phoneNumber && appVerifier) {
        console.log(appVerifier);
        firebase
          .auth()
          .signInWithPhoneNumber(this.state.phoneNumber, appVerifier)
          .then(async confirmationResult => {
            console.log("conf res");
            console.log(confirmationResult);
            if (confirmationResult) {
              this.setState({ isCodeSent: true });
              console.log("send code successfully");
            } else {
              this.setState({ isCodeSent: false });
              console.log("error to send code");
            }
            await this.setState({
              confirmationResult
            });
          })
          .catch(error => {
            this.setState({ isCodeSent: false });
            console.log("error to send code");
          });
      } else {
        this.setState({
          isCodeSent: false
        });
        console.log("error to send code");
      }
    } else {
      console.log("phone number should be minimum 8 digit");
    }
  };

  //clicking the retrival email for login
  handleEmailClick = item => {
    console.log(item);
    this.props.handleForgotEmail(item);
  };

  //to match otp code
  matchVCode = () => {
    console.log(this.state.confirmationResult);
    if (this.state.confirmationResult) {
      this.state.confirmationResult
        .confirm(this.state.vCode)
        .then(res => {
          console.log(res);
          console.log("phone verification successful");
          this.setState({ isVerifySuccess: true });
          this.displayPhoneData();
        })
        .catch(err => {
          console.log("verification un-successful/ wrong OTP");
          console.log(err);
          this.setState({
            isVerifySuccess: false
          });
        });
    } else {
      //there might be 2 reason for the error
      console.log(
        "no confirmation res/ Captcha not completed or Wrong OTP entered"
      );
      this.setState({
        isVerifySuccess: false
      });
    }
  };

  render() {
    return (
      <React.Fragment>
        <h1 className="large bold title-margin">Forgot Email</h1>
        <div
          className="captcha-pos"
          ref={capt => (this.recapt = capt)}
          id="recaptcha-container"
        />

        {!this.state.isCodeSent && (
          <div>
            <div>
              <span>Enter phone number here:</span>
              <br />
              <input onChange={this.handlePhoneNumber} />
            </div>
            <button onClick={this.sendCode}>
              Send Code
            </button>
          </div>
        )}

        {this.state.isCodeSent && !this.state.isVerifySuccess && (
          <div>
            <br />
            <span>Enter Verfication code here:</span>
            <br />
            <input onChange={this.handleVCode} />
            <br />
            <button onClick={this.matchVCode}>Submit and match vCode</button>
            <br />
            <br />
          </div>
        )}

        <div>
          <ul>
            {this.state.mailList.map((item, i) => {
              return (
                <li key={i} onClick={() => this.handleEmailClick(item)}>
                  {item}
                </li>
              );
            })}
          </ul>
        </div>
      </React.Fragment>
    );
  }
}
